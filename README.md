# win10LTSC2021AddAppStore

#### v0.0.2

#### 介绍
1.  用于Win10 LTSC 2021版本添加应用商店功能；
2.  可从根本解决wsappx进程(即AppXSvc服务)占用过高的问题；
3.  或许可以解决“StateRepository-Machine.srd-wal”文件体积不断增加的问题；

#### 使用说明
1.  本方法仅在Win10 LTSC 2021 x64版本系统上测试过，使用正常；
2.  下方推荐的安装包版本于2022.05.31测试通过，可正常安装，未报错；
3.  若网页打开较慢，可直接使用"Packages"目录下的安装包，仅限64位系统；
4.  32位系统需要下载以下包对应的"x86"版本即可(本人暂无32位系统，未测试)；


#### 安装教程

1.  打开网页[Online link generator for Microsoft Store](https://store.rg-adguard.net/)
2.  在左侧的"Select your request type"中选择"PackageFamilyName"
3.  然后搜索"Microsoft.WindowsStore_8wekyb3d8bbwe"
4.  下载以下5个安装包(见下文)
5.  下载完成后将其放入一个文件夹内(尽量使用英文路径)
6.  使用管理员权限打开"PowerShell"，切换路径至上述文件夹内(例：cd 'D:\AppXPackages')
7.  使用'Add-AppxPackage \*'命令安装目录下所有包
8.  全部安装完成后重启即可

#### 需下载的包(x64)

1.  Microsoft.NET.Native.Framework.2.2_2.2.29512.0_x64__8wekyb3d8bbwe.Appx
2.  Microsoft.NET.Native.Runtime.2.2_2.2.28604.0_x64__8wekyb3d8bbwe.Appx
3.  Microsoft.UI.Xaml.2.4_2.42007.9001.0_x64__8wekyb3d8bbwe.Appx
4.  Microsoft.VCLibs.140.00_14.0.30704.0_x64__8wekyb3d8bbwe.Appx
5.  Microsoft.WindowsStore_12107.1001.15.0_neutral___8wekyb3d8bbwe.AppxBundle

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
